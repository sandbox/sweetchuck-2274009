<?php
/**
 * @file
 * Plugin definition and callbacks for a panels:styles plugin.
 *
 * @ingroup PanelsPlugin PanelsPluginStyles
 */

$plugin = array(
  'title' => t('Layouts: Amoeba'),

  'defaults' => array(
    'region' => array(
      'pane_separator' => array(
        'name' => 'hr',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
    ),
    'pane' => array(
      'main' => array(
        'name' => 'div',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
      'title' => array(
        'name' => 'h2',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
      'feeds' => array(
        'name' => 'div',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
      'content' => array(
        'name' => 'div',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
      'links' => array(
        'name' => 'div',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
      'more' => array(
        'name' => 'div',
        'attributes' => array(
          'class' => array(
            'value' => array(),
            'replace_keywords' => FALSE,
          ),
        ),
      ),
    ),
  ),

  'settings form'      => 'layouts_panels_styles_amoeba_region_settings_form',
  'render region'      => 'layouts_panels_styles_amoeba_region',

  'pane settings form' => 'layouts_panels_styles_amoeba_pane_settings_form',
  'render pane'        => 'layouts_panels_styles_amoeba_pane',
);

/**
 * Implements "settings form" callback of a panels:styles plugin.
 *
 * @param array $settings
 *   Saved settings of this style or an empty array.
 * @param panels_display $panels_display
 *   Panels display object.
 * @param string $region_id
 *   Machine name of the region in the layout.
 * @param string $box_type
 *   The value can be "display", "region" or "pane".
 * @param array $form_state
 *   Form API form state array.
 *
 * @return array
 *   Form API render array.
 */
function layouts_panels_styles_amoeba_region_settings_form(array $settings, panels_display $panels_display, $region_id, $box_type, array &$form_state) {
  $separator_states = array(
    'invisible' => array(
      ':input[name$="[region][pane_separator][name]"]' => array('value' => ''),
    ),
  );

  return array(
    'region' => array(
      '#tree' => TRUE,
      'pane_separator' => array(
        '#tree' => TRUE,
        'name' => array(
          '#type' => 'select',
          '#title' => t('Pane separator'),
          '#default_value' => $settings['region']['pane_separator']['name'],
          '#empty_option' => t('- Select -'),
          '#options' => array(
            'div' => 'div',
            'span' => 'span',
            'hr' => 'hr',
          ),
        ),
        'attributes' => array(
          '#type' => 'fieldset',
          '#tree' => TRUE,
          '#title' => t('Attributes'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#states' => $separator_states,
          '#theme' => 'layouts_tag_attributes',
          'class' => array(
            '#tree' => TRUE,
            'value' => array(
              '#type' => 'textfield',
              '#title' => t('Class'),
              '#default_value' => implode(' ', $settings['region']['pane_separator']['attributes']['class']['value']),
              '#element_validate' => array(
                'layouts_element_validate_attributes_class',
              ),
            ),
            'replace_keywords' => array(
              '#type' => 'checkbox',
              '#title' => t('Replace context keyword substitutes'),
              '#default_value' => $settings['region']['pane_separator']['attributes']['class']['replace_keywords'],
            ),
          ),
        ),
      ),
    ),
  );
}

/**
 * Implements template_preprocess_layouts_panels_styles_amoeba_region().
 *
 * @see template_process_layouts_panels_styles_amoeba_region()
 * @see theme_layouts_panels_styles_amoeba_region()
 *
 * @ingroup themeable
 */
function template_preprocess_layouts_panels_styles_amoeba_region(&$vars) {
  $s =& $vars['settings']['region'];

  $key = 'pane_separator_attributes_array';
  if ($s['pane_separator']['name']) {
    _layouts_preprocess_attributes($s['pane_separator']['attributes'], $vars['display']->context, $vars[$key]);
  }

  if (!isset($vars[$key])) {
    $vars[$key] = array();
  }
}

/**
 * Implements template_process_layouts_panels_styles_amoeba_region().
 *
 * @see template_preprocess_layouts_panels_styles_amoeba_region()
 * @see theme_layouts_panels_styles_amoeba_region()
 *
 * @ingroup themeable
 */
function template_process_layouts_panels_styles_amoeba_region(&$vars) {
  $name = $vars['settings']['region']['pane_separator']['name'];

  $vars['pane_separator'] = '';
  if ($name) {
    $vars['pane_separator_attributes'] = drupal_attributes($vars['pane_separator_attributes_array']);
    $vars['pane_separator'] = '<' . $name . $vars['pane_separator_attributes'] . ($name == 'hr' ? ' />' : '></' . $name . '>');
  }
}

/**
 * Implements theme_layouts_panels_styles_amoeba_region().
 *
 * Render contents of a panels layout region.
 *
 * @param array $vars
 *   Keys:
 *   - display:   panels_display
 *   - owner_id:  string         - Owner of the region. Machine name of the
 *                                 panel or mini panel or something else.
 *   - panes:     string[]       - Rendered panes.
 *   - settings:  array          - Custom settings of this style plugin.
 *   - region_id: string         - Machine name of the region in the layout.
 *   - style:     array          - Info array of this style plugin.
 *
 * @return string
 *   Themed HTML string.
 *
 * @see template_preprocess_layouts_panels_styles_amoeba_region()
 * @see template_process_layouts_panels_styles_amoeba_region()
 *
 * @ingroup themeable
 */
function theme_layouts_panels_styles_amoeba_region(array $vars) {
  return implode($vars['pane_separator'], $vars['panes']);
}

/**
 * Settings form of "Layouts: Amoeba" panel style in pane context.
 *
 * @param array $settings
 *   Style settings.
 * @param panels_display $panels_display
 *   Panels display object.
 * @param string $pane_id
 *   Name of the pane.
 * @param string $box_type
 *   In most cases this is equal to 'pane'.
 * @param array $form_state
 *   Form API form state array.
 *
 * @return array
 *   Form API render array.
 */
function layouts_panels_styles_amoeba_pane_settings_form(array $settings, panels_display $panels_display, $pane_id, $box_type, array &$form_state) {
  $tags = array(
    // @todo Remove the main][attributes][class.
    'main' => array('title' => t('Main')),
    'title' => array('title' => t('Title')),
    'feeds' => array('title' => t('Feeds')),
    'content' => array('title' => t('Content')),
    'links' => array('title' => t('Links')),
    'more' => array('title' => t('More')),
  );

  $element = array(
    'pane' => array(
      '#tree' => TRUE,
    ),
  );

  foreach ($tags as $tag => $tag_info) {
    $states = array(
      'invisible' => array(
        ':input[name$="[pane][' . $tag . '][name]"]' => array('value' => ''),
      ),
    );

    $element['pane'][$tag] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $tag_info['title'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'name' => array(
        '#type' => 'select',
        '#title' => t('Name'),
        '#default_value' => $settings['pane'][$tag]['name'],
        '#empty_option' => t('- Select -'),
        '#options' => array(
          t('Neutral') => array(
            'div' => 'div',
            'span' => 'span',
          ),
          t('Semantic') => array(
            'article' => 'article',
            'aside' => 'aside',
            'dialog' => 'dialog',
            'footer' => 'footer',
            'header' => 'header',
            'main' => 'main',
            'nav' => 'nav',
            'section' => 'section',
          ),
        ),
      ),
      'attributes' => array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => t('Attributes'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#states' => $states,
        '#theme' => 'layouts_tag_attributes',
        'class' => array(
          '#tree' => TRUE,
          'value' => array(
            '#type' => 'textfield',
            '#title' => t('Class'),
            '#default_value' => implode(' ', $settings['pane'][$tag]['attributes']['class']['value']),
            '#element_validate' => array(
              'layouts_element_validate_attributes_class',
            ),
          ),
          'replace_keywords' => array(
            '#type' => 'checkbox',
            '#title' => t('Replace context keyword substitutes'),
            '#default_value' => $settings['pane'][$tag]['attributes']['class']['replace_keywords'],
          ),
        ),
      ),
    );
  }

  $element['pane']['title']['name']['#options'] = array(
    t('Neutral') => array(
      'div' => 'div',
      'span' => 'span',
    ),
    t('Semantic') => array(
      'h1' => 'h1',
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
      'h5' => 'h5',
      'h6' => 'h6',
    ),
  );

  return $element;
}

/**
 * Implements template_preprocess_layouts_panels_styles_amoeba_pane().
 *
 * Clean up the panel pane variables for the template.
 *
 * @see template_process_layouts_panels_styles_amoeba_pane()
 * @see theme_layouts_panels_styles_amoeba_pane()
 *
 * @ingroup themeable
 */
function template_preprocess_layouts_panels_styles_amoeba_pane(&$vars, $hook) {
  ctools_include('cleanstring');

  $s =& $vars['settings']['pane'];

  $names = array(
    'main',
    'title',
    'feeds',
    'content',
    'links',
    'more',
  );
  foreach ($names as $name) {
    $vars["{$name}_tag"] = $s[$name]['name'];
    $key = ($name === 'main' ? 'attributes_array' : "{$name}_attributes_array");
    _layouts_preprocess_attributes($s[$name]['attributes'], $vars['display']->context, $vars[$key]);

    if (!isset($vars[$key])) {
      $vars[$key] = array();
    }
  }

  $content =& $vars['content'];

  $content_type_clean = ctools_cleanstring($content->type, array('lower case' => TRUE));
  $content_subtype_clean = ctools_cleanstring($content->subtype, array('lower case' => TRUE));
  $content_type_tpl = strtr($content_type_clean, '-', '_');
  $content_subtype_tpl = strtr($content_subtype_clean, '-', '_');

  $vars['contextual_links'] = array();
  $vars['classes_array'] = array();
  $vars['admin_links'] = '';

  if (module_exists('contextual') && user_access('access contextual links')) {
    $links = array();
    // These are specified by the content.
    if (!empty($content->admin_links)) {
      $links += $content->admin_links;
    }

    // Take any that may have been in the render array we were given and
    // move them up so they appear outside the pane properly.
    if (is_array($content->content) && isset($content->content['#contextual_links'])) {
      $element = array(
        '#type' => 'contextual_links',
        '#contextual_links' => $content->content['#contextual_links'],
      );
      unset($content->content['#contextual_links']);

      // Add content to $element array.
      $element['#element'] = $content->content;

      $element = contextual_pre_render_links($element);
      if (!empty($element['#links'])) {
        $links += $element['#links'];
      }
    }

    if ($links) {
      $build = array(
        '#prefix' => '<div class="contextual-links-wrapper">',
        '#suffix' => '</div>',
        '#theme' => 'links__contextual',
        '#links' => $links,
        '#attributes' => array('class' => array('contextual-links')),
        '#attached' => array(
          'library' => array(array('contextual', 'contextual-links')),
        ),
      );
      $vars['attributes_array']['class'][] = 'contextual-links-region';
      $vars['admin_links'] = drupal_render($build);
    }
  }

  $vars['attributes_array']['class'][] = 'panel-pane';

  // Add some usable classes based on type/subtype
  $type_class = $content_type_clean ? 'pane-' . $content_type_clean : '';
  $subtype_class = $content_subtype_clean ? 'pane-' . $content_subtype_clean : '';

  // Sometimes type and subtype are the same. Avoid redundant classes.
  $vars['attributes_array']['class'][] = $type_class;
  if ($type_class != $subtype_class) {
    $vars['attributes_array']['class'][] = $subtype_class;
  }

  // Add id and custom class if sent in.
  if (!empty($content->css_id)) {
    $vars['attributes_array']['id'] = $content->css_id;
  }

  if (!empty($content->css_class)) {
    $vars['attributes_array']['class'][] = $content->css_class;
  }

  // Add template file suggestion for content type and sub-type.
  $vars['theme_hook_suggestions'][] = "{$hook}__{$content_type_tpl}";
  $vars['theme_hook_suggestions'][] = "{$hook}__{$content_type_tpl}__{$content_subtype_tpl}";

  $vars['pane_prefix'] = !empty($content->pane_prefix) ? $content->pane_prefix : '';
  $vars['pane_suffix'] = !empty($content->pane_suffix) ? $content->pane_suffix : '';

  $vars['title'] = !empty($content->title) ? $content->title : '';
  $vars['title_attributes_array']['class'][] = 'pane-title';

  $vars['feeds'] = !empty($content->feeds) ? implode(' ', $content->feeds) : '';

  $vars['content'] = !empty($content->content) ? $content->content : '';
  $vars['content_attributes_array']['class'][] = 'pane-content';

  $vars['links'] = !empty($content->links) ? theme('links', array('links' => $content->links)) : '';

  $vars['more'] = '';
  if (!empty($content->more)) {
    if (empty($content->more['title'])) {
      $content->more['title'] = t('more');
    }
    $vars['more'] = l($content->more['title'], $content->more['href'], $content->more);
  }
}

/**
 * Implements template_process_layouts_panels_styles_amoeba_pane().
 *
 * @see template_preprocess_layouts_panels_styles_amoeba_pane()
 * @see theme_layouts_panels_styles_amoeba_pane()
 *
 * @ingroup themeable
 */
function template_process_layouts_panels_styles_amoeba_pane(&$vars, $hook) {
  $s =& $vars['settings']['pane'];

  $tags = array(
    'main',
    'title',
    'feeds',
    'content',
    'links',
    'more',
  );

  foreach ($tags as $tag) {
    if ($s[$tag]['name']) {
      $attr_key = ($tag === 'main' ? 'attributes' : "{$tag}_attributes");
      $vars[$attr_key] = drupal_attributes($vars["{$attr_key}_array"]);
      $vars["{$tag}_tag_open"] = '<' . $s[$tag]['name'] . $vars[$attr_key] . '>';
      $vars["{$tag}_tag_close"] = '</' . $s[$tag]['name'] . '>';
    }
    else {
      $vars["{$tag}_tag_open"] = '';
      $vars["{$tag}_tag_close"] = '';
    }
  }
}

/**
 * Implements theme_layouts_panels_styles_amoeba_pane().
 *
 * @see template_preprocess_layouts_panels_styles_amoeba_pane()
 * @see template_process_layouts_panels_styles_amoeba_pane()
 *
 * @ingroup themeable
 */
function theme_layouts_panels_styles_amoeba_pane($vars) {
  $output = '';

  $output .= $vars['pane_prefix'];
  $output .= $vars['main_tag_open'];

  $output .= $vars['admin_links'];

  $output .= render($vars['title_prefix']);
  if ($vars['title']) {
    $output .= $vars['title_tag_open'] . $vars['title'] . $vars['title_tag_close'];
  }
  $output .= render($vars['title_suffix']);

  if ($vars['feeds']) {
    $output .= $vars['feeds_tag_open'] . $vars['feeds'] . $vars['feeds_tag_close'];
  }

  $output .= $vars['content_tag_open'] . render($vars['content']) . $vars['content_tag_close'];

  if ($vars['links']) {
    $output .= $vars['links_tag_open'] . $vars['links'] . $vars['links_tag_close'];
  }

  if ($vars['more']) {
    $output .= $vars['more_tag_open'] . $vars['more'] . $vars['more_tag_close'];
  }

  $output .= $vars['main_tag_close'];
  $output .= $vars['pane_suffix'];

  return $output;
}
