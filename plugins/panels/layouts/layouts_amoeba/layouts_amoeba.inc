<?php
/**
 * @file
 * Plugin definition and callbacks for a panels:layouts Amoeba plugin.
 *
 * @ingroup PanelsPlugin PanelsPluginLayouts
 */

$plugin = array(
  'path' => '.',
  'file' => drupal_get_path('module', 'layouts') . '/plugins/panels/layouts/layouts_amoeba/layouts_amoeba.inc',

  'category' => t('Amoeba'),
  'title' => t('Parent'),
  'description' => t('Configurable markup.'),
  'theme' => 'layouts_panels_layouts_amoeba',

  'get child'    => 'layouts_panels_layouts_amoeba_get_child',
  'get children' => 'layouts_panels_layouts_amoeba_get_children',

  'settings form'     => 'layouts_panels_layouts_amoeba_settings_form',
  'settings validate' => 'layouts_panels_layouts_amoeba_settings_form_validate',
  'settings submit'   => 'layouts_panels_layouts_amoeba_settings_form_submit',
);

/**
 * Implements "get child" callback of a panels:layouts plugin.
 *
 * @param array $plugin
 *   Original plugin definition.
 * @param string $layout_name
 *   Name of the parent layout.
 * @param string $child_layout_name
 *   Name of the requested child layout.
 *
 * @return array|null
 *   Definition array of the child layout or NULL.
 */
function layouts_panels_layouts_amoeba_get_child(array $plugin, $layout_name, $child_layout_name) {
  $children = layouts_panels_layouts_amoeba_get_children($plugin, $layout_name);
  $name = "layouts_amoeba:$child_layout_name";

  return isset($children[$name]) ? $children[$name] : NULL;
}

/**
 * Implements "get children" callback of a panels:layouts plugin.
 *
 * @param array $plugin
 *   Original plugin definition.
 * @param string $layout_name
 *   Name of the parent layout.
 *
 * @return array
 *   Array of child layout definitions.
 */
function layouts_panels_layouts_amoeba_get_children(array $plugin, $layout_name) {
  $layouts = array();

  $children = layouts_info_amoeba_layout_children();
  foreach ($children as $child_name => &$child) {
    $name = "layouts_amoeba:{$child_name}";
    $layouts[$name] = layouts_panels_layouts_amoeba_plugin_merge($plugin, $child_name, $child);
  }

  return $layouts;
}

/**
 * Implements "settings_form" callback of a panels:layouts plugin.
 *
 * @param panels_display $display
 *   Panels display object.
 * @param array $layout
 *   Original plugin definition.
 * @param array $settings
 *   Current settings of the layout.
 *
 * @return array
 *   Form API render array.
 */
function layouts_panels_layouts_amoeba_settings_form(panels_display $display, array $layout, array $settings) {
  if (!isset($settings['child_name']) || $settings['child_name'] != $layout['child_name']) {
    $settings['child_name'] = $layout['child_name'];
  }

  $element = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Layout settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array(
      'class' => array('layouts-settings'),
    ),
    'child_name' => array(
      '#type' => 'value',
      '#value' => $layout['child_name'],
    ),
    'layout' => array(
      '#type' => 'fieldset',
      '#title' => t('Info'),
      '#weight' => -20,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array(
        'class' => array('layouts-meta', 'clearfix'),
      ),
      'icon' => array(
        '#type' => 'item',
        '#title' => t('Icon'),
        '#markup' => panels_print_layout_icon('', $layout),
      ),
      'category' => array(
        '#type' => 'item',
        '#title' => t('Category'),
        '#markup' => '<div>' . check_plain($layout['category']) . '</div>',
      ),
      'title' => array(
        '#type' => 'item',
        '#title' => t('Title'),
        '#markup' => '<div>' . check_plain($layout['title']) . '</div>',
      ),
      'name' => array(
        '#type' => 'item',
        '#title' => t('Machine name'),
        '#markup' => '<div>' . $layout['name'] . '</div>',
      ),
      'description' => array(
        '#type' => 'item',
        '#title' => t('Description'),
        '#markup' => '<div>' . check_plain($layout['description']) . '</div>',
      ),
      'markup' => array(
        '#theme' => 'layouts_amoeba_layout_settings',
        '#layout' => $layout,
        '#settings' => $layout['defaults'],
      ),
    ),
  );

  if (isset($layout['layouts_icons'])) {
    $icon_path = $layout['path'] . '/' . $layout['icon'];
    foreach ($layout['layouts_icons'] as $icon_name => $icon) {
      if ($icon_name === 'default') {
        continue;
      }

      if (!isset($element['layout']['icons'])) {
        $element['layout']['icons'] = array(
          '#theme' => 'table',
          '#sticky' => FALSE,
          '#header' => array(),
          '#rows' => array(),
        );
      }

      $icon_path_variant = preg_replace('/\.svg$/', "-{$icon_name}.svg", $icon_path);
      $element['layout']['icons']['#header'][] = $icon['title'];
      $element['layout']['icons']['#rows'][0][] = theme(
        'image',
        array(
          'path' => $icon_path_variant,
          'title' => $icon['title'],
        )
      );
    }
  }

  return $element;
}

/**
 * Implements hook_preprocess_layouts_panels_layouts_amoeba().
 *
 * Add markup property variables, such as HTML tag name and attributes.
 * The "process" phase does not convert the attributes array to string because
 * much more easier to add the "empty" classes when the layout gets rendered.
 *
 * @ingroup themeable
 */
function template_preprocess_layouts_panels_layouts_amoeba(&$vars, $hook) {
  // Prepare the required variables. To make it possible to call this theme
  // function without Panels.
  // - display
  // - renderer.
  if (!isset($vars['display'])) {
    $vars['display'] = (object) array('context' => array());
  }

  if (!isset($vars['renderer'])) {
    $vars['renderer'] = (object) array('admin' => FALSE);
  }

  // Always override the settings because there is no UI for the layout
  // settings.
  $vars['settings'] = $vars['layout']['defaults'];

  // On the layout switching form the newly selected layout being rendered for
  // preview, but the settings is still belongs to the previous layout.
  if (!isset($vars['settings']['child_name'])
    || $vars['settings']['child_name'] != $vars['layout']['child_name']
  ) {
    $vars['settings']['child_name'] = $vars['layout']['child_name'];
  }

  if (empty($vars['theme_hook_suggestions'])) {
    $vars['theme_hook_suggestions'][] = $hook;
  }

  $vars['theme_hook_suggestions'][] = "{$hook}__{$vars['layout']['child_name']}";

  if (!empty($vars['css_id'])) {
    $vars['settings']['main']['options']['outer']['tag']['attributes']['id'] = array(
      'value' => $vars['css_id'],
      'replace_keywords' => (strpos($vars['css_id'], '%') !== FALSE),
    );
  }

  if ($vars['renderer']->admin) {
    $vars['settings']['main']['options']['outer']['tag']['attributes']['class']['value'][] = 'admin';
  }

  _template_preprocess_layouts_panels_layouts_amoeba_main($vars, array('main'), $vars['settings']['main']);
}

/**
 * Render a child layout of layouts_amoeba.
 *
 * @param array $vars
 *   Theme variables.
 *
 * @return string
 *   Rendered HTML string.
 *
 * @see panels_renderer_standard::render_layout()
 */
function theme_layouts_panels_layouts_amoeba(array $vars) {
  return _theme_layouts_panels_layouts_amoeba_main($vars, 'main', $vars['settings']['main']);
}

/**
 * Helper function to initialize the child layout plugin properties.
 *
 * @param array $plugin
 *   Original plugin definition.
 * @param string $child_name
 *   Name of the child layout.
 * @param array $child
 *   Child layout definition.
 *
 * @return array
 *   Extended child layout definition.
 */
function layouts_panels_layouts_amoeba_plugin_merge(array $plugin, $child_name, array $child) {
  $child = drupal_array_merge_deep($plugin, $child);
  $child['layout'] = $plugin;
  $child['name'] = "layouts_amoeba:$child_name";
  $child['child_name'] = $child_name;
  $child['defaults']['child_name'] = $child_name;

  return $child;
}

/**
 * Add markup property variables.
 *
 * @param array $vars
 *   Theme variables.
 * @param array $parents
 *   Array of parent names.
 * @param array $settings
 *   Settings of the current HTML tag.
 */
function _template_preprocess_layouts_panels_layouts_amoeba_main(array &$vars, array $parents, array $settings) {
  $name = end($parents);
  $type = isset($settings['children']) ? 'wrapper' : 'region';

  foreach (array('outer', 'inner') as $position) {
    $vars["{$name}_{$position}_tag"] = $settings['options'][$position]['tag']['name'];
    $key = "{$name}_{$position}_attributes_array";
    _layouts_preprocess_attributes($settings['options'][$position]['tag']['attributes'], $vars['display']->context, $vars[$key]);
    if (!isset($vars[$key])) {
      $vars[$key] = array();
    }

    if (!isset($vars[$key])) {
      $vars[$key] = array();
    }
  }

  if ($type === 'wrapper') {
    foreach ($settings['children'] as $child_name => $child) {
      $child_parents = $parents;
      $child_parents[] = $child_name;
      _template_preprocess_layouts_panels_layouts_amoeba_main($vars, $child_parents, $child);
    }
  }
}

/**
 * Render a child layout of layouts_amoeba.
 */
function _theme_layouts_panels_layouts_amoeba_main(array $vars, $name, array $settings) {
  $content = '';
  if (isset($settings['children'])) {
    uasort($settings['children'], 'drupal_sort_weight');
    foreach ($settings['children'] as $child_name => $child) {
      $sub_content = _theme_layouts_panels_layouts_amoeba_main($vars, $child_name, $child);
      if ($sub_content) {
        $content .= $sub_content;
      }
      else {
        $vars["{$name}_outer_attributes_array"]['class'][] = "empty-" . drupal_html_class($child_name);
      }
    }
  }
  else {
    $content .= $vars['content'][$name];
    if (!$content) {
      $vars["{$name}_outer_attributes_array"]['class'][] = 'empty';
    }
  }

  $prefix = '';
  $suffix = '';
  if ($vars["{$name}_outer_tag"] && ($content || $settings['options']['outer']['render_empty'])) {
    $prefix = '<' . $vars["{$name}_outer_tag"] . drupal_attributes($vars["{$name}_outer_attributes_array"]) . '>';
    $suffix = '</' . $vars["{$name}_outer_tag"] . '>';

    if ($vars["{$name}_inner_tag"] && ($content || $settings['options']['inner']['render_empty'])) {
      $prefix .= '<' . $vars["{$name}_inner_tag"] . drupal_attributes($vars["{$name}_inner_attributes_array"]) . '>';
      $suffix = '</' . $vars["{$name}_inner_tag"] . '>' . $suffix;
    }
  }

  return $prefix . $content . $suffix;
}
