<?php
/**
 * @file
 * API documentation for Layouts module.
 */

/**
 * Define child layouts for panels:layouts:layouts_amoeba plugin.
 *
 * @see layouts_amoeba_layout_child_settings_main_defaults()
 */
function hook_layouts_amoeba_layout_preset_info() {
  return array(
    // The key is unique and prefixed with the module name.
    // The structure is same as a panels:layouts plugin definition.
    'my_module_two_col_stacked' => array(
      // Layout settings.
      // Required
      'defaults' => array(
        // The "main" key is required.
        // The DOM hierarchy is custom, but must contains as many regions as
        // many is indicated in the "num_of_regions" property (see above).
        // The region names are r1, r2, r3 ... rN+1.
        'main' => array(
          // Every wrapper and region contains an "options" section like this,
          // but only this one is expanded.
          'options' => array(
            // The "outer" and "inner" are fixed keys, the "layouts" module
            // handle only these keys.
            // A wrapper or a region can have an outer and an inner HTMLElement.
            // If you want to use the "inner" then the "outer" is mandatory.
            'outer' => array(
              // If set to TRUE then this wrapper tag will be rendered even if
              // the content is empty.
              'render_empty' => TRUE,
              'tag' => array(
                // Name of any valid HTML tag, such as 'div' or 'header'.
                'name' => 'div',
                'attributes' => array(
                  // Key can be any valid HTML attribute name.
                  'class' => array(
                    'value' => array(),
                    // Set to TRUE if you want replace the context keywords with
                    // their values. Usually the keywords start with '%'.
                    'replace_keywords' => FALSE,
                  ),
                ),
              ),
            ),
            'inner' => array(
              'render_empty' => TRUE,
              'tag' => array(
                'name' => '',
                'attributes' => array(
                  'class' => array(
                    'value' => array(),
                    'replace_keywords' => FALSE,
                  ),
                ),
              ),
            ),
          ),
          'children' => array(
            // This is a region, because this have no "children".
            'r1' => array(
              // 'options' => array(),
            ),
            // The "center" is a custom name.
            // This is a wrapper, because this have "children".
            'center' => array(
              // 'options' => array(),
              'children' => array(
                'r2' => array(
                  // 'options' => array(),
                ),
                'r3' => array(
                  // 'options' => array(),
                ),
              ),
            ),
            'r4' => array(
              // 'options' => array(),
            ),
          ),
        ),
      ),

      // Keys are the region identifiers.
      // Optional.
      'layouts_icons' => array(
        // Required.
        'default' => array(
          'title' => t('Default'),
          'settings' => array(
            'width' => 54,
            'height' => 72,
          ),
          'boxes' => array(
            'r1' => array(
              // Position and the size of the region.
              // - x: 0 = Top.
              // - y: 0 = Left.
              'x' => 0,
              'y' => 0,
              'width' => 4,
              'height' => 1,
            ),
            'r2' => array(
              'x' => 0,
              'y' => 1,
              'width' => 3,
              'height' => 3,
            ),
            'r3' => array(
              'x' => 3,
              'y' => 1,
              'width' => 1,
              'height' => 3,
            ),
            'r4' => array(
              'x' => 0,
              'y' => 4,
              'width' => 4,
              'height' => 1,
            ),
          ),
        ),
      ),
    ),
  );
}
