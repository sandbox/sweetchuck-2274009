<?php
/**
 * @file
 * Layouts hooks.
 */

/**
 * Implements hook_layouts_amoeba_layout_child_info().
 */
function layouts_layouts_amoeba_layout_child_info() {
  $path = drupal_get_path('module', 'layouts');

  return array(
    'layouts_r4_two_col_stacked' => array(
      'category' => t('Amoeba example'),
      'title' => t('Layouts - Two columns stacked'),
      'icon' => "$path/images/layouts.layouts-amoeba.layouts-r4-two-col-stacked.svg",
      'css' => array(
        "$path/css/layouts.layouts-amoeba.layouts-r4-two-col-stacked.css",
      ),
      'admin css' => array(
        "$path/css/layouts.layouts-amoeba.layouts-r4-two-col-stacked.admin.css",
      ),
      'regions' => array(
        'r1' => t('Region 1'),
        'r2' => t('Region 2'),
        'r3' => t('Region 3'),
        'r4' => t('Region 4'),
      ),
      'defaults' => array(
        'main' => array(
          'options' => array(
            'outer' => array(
              'tag' => array(
                'name' => 'div',
                'attributes' => array(
                  'class' => array(
                    'value' => array('layouts-r4-two-col-stacked'),
                  ),
                ),
              ),
            ),
          ),
          'children' => array(
            'r1' => array(
              'options' => array(
                'outer' => array(
                  'tag' => array(
                    'name' => 'header',
                    'attributes' => array(
                      'class' => array(
                        'value' => array(),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            'center' => array(
              'options' => array(
                'outer' => array(
                  'tag' => array(
                    'name' => 'div',
                    'attributes' => array(
                      'class' => array(
                        'value' => array('clearfix'),
                      ),
                    ),
                  ),
                ),
              ),
              'children' => array(
                'r2' => array(
                  'options' => array(
                    'outer' => array(
                      'tag' => array(
                        'name' => 'main',
                        'attributes' => array(
                          'class' => array(
                            'value' => array(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                'r3' => array(
                  'options' => array(
                    'outer' => array(
                      'tag' => array(
                        'name' => 'aside',
                        'attributes' => array(
                          'class' => array(
                            'value' => array(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            'r4' => array(
              'options' => array(
                'outer' => array(
                  'tag' => array(
                    'name' => 'footer',
                    'attributes' => array(
                      'class' => array(
                        'value' => array(),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      'layouts_icons' => array(
        'default' => array(
          'title' => t('Default'),
          'settings' => array(
            'width' => 54,
            'height' => 72,
          ),
          'boxes' => array(
            'r1' => array(
              'x' => 0,
              'y' => 0,
              'width' => 4,
              'height' => 1,
            ),
            'r2' => array(
              'x' => 0,
              'y' => 1,
              'width' => 3,
              'height' => 3,
            ),
            'r3' => array(
              'x' => 3,
              'y' => 1,
              'width' => 1,
              'height' => 3,
            ),
            'r4' => array(
              'x' => 0,
              'y' => 4,
              'width' => 4,
              'height' => 1,
            ),
          ),
        ),
        'desktop' => array(
          'title' => t('Desktop'),
          'settings' => array(
            'width' => 54,
            'height' => 72,
          ),
          'boxes' => array(
            'r1' => array(
              'x' => 0,
              'y' => 0,
              'width' => 4,
              'height' => 1,
            ),
            'r2' => array(
              'x' => 0,
              'y' => 1,
              'width' => 3,
              'height' => 3,
            ),
            'r3' => array(
              'x' => 3,
              'y' => 1,
              'width' => 1,
              'height' => 3,
            ),
            'r4' => array(
              'x' => 0,
              'y' => 4,
              'width' => 4,
              'height' => 1,
            ),
          ),
        ),
        'mobile-portrait' => array(
          'title' => t('Mobile - Portrait'),
          'settings' => array(
            'width' => 54,
            'height' => 72,
          ),
          'boxes' => array(
            'r1' => array(
              'x' => 0,
              'y' => 0,
              'width' => 1,
              'height' => 1,
            ),
            'r2' => array(
              'x' => 0,
              'y' => 1,
              'width' => 1,
              'height' => 1,
            ),
            'r3' => array(
              'x' => 0,
              'y' => 2,
              'width' => 1,
              'height' => 1,
            ),
            'r4' => array(
              'x' => 0,
              'y' => 3,
              'width' => 1,
              'height' => 1,
            ),
          ),
        ),
        'mobile-landscape' => array(
          'title' => t('Mobile - Landscape'),
          'settings' => array(
            'width' => 72,
            'height' => 54,
          ),
          'boxes' => array(
            'r1' => array(
              'x' => 0,
              'y' => 0,
              'width' => 1,
              'height' => 1,
            ),
            'r2' => array(
              'x' => 0,
              'y' => 1,
              'width' => 1,
              'height' => 1,
            ),
            'r3' => array(
              'x' => 0,
              'y' => 2,
              'width' => 1,
              'height' => 1,
            ),
            'r4' => array(
              'x' => 0,
              'y' => 3,
              'width' => 1,
              'height' => 1,
            ),
          ),
        ),
      ),
    ),
  );
}
