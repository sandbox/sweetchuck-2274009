
# Require any additional compass plugins here.
require 'compass/import-once/activate'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "css-src"
images_dir = "images"
javascripts_dir = "js"

relative_assets = true

is_production = (environment == :production)

output_style = is_production ? :compressed : :expanded
line_comments = !is_production
sourcemap = !is_production
debug_info = !is_production
