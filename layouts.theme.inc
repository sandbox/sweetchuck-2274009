<?php
/**
 * @file
 * Theme layer related functions.
 */

/**
 * Implements theme_layouts_tag_attributes().
 *
 * @ingroup themeable
 */
function theme_layouts_tag_attributes($vars) {
  $e =& $vars['element'];

  $table = array(
    'sticky' => FALSE,
    'header' => array(
      'name' => array('data' => t('Name')),
      'value' => array('data' => t('Value')),
      'replace_keywords' => array('data' => t('Replace keywords')),
    ),
    'rows' => array(),
  );

  if (!empty($e['#title']) && !isset($e['#title_display']) || $e['#title_display'] != 'none') {
    $table['caption'] = $e['#title'];
  }

  if (!empty($e['#attributes'])) {
    $table['attributes'] = $e['#attributes'];
  }

  foreach (element_children($e) as $key) {
    if (!isset($e[$key]['value'])) {
      continue;
    }
    $e[$key]['value']['#title_display'] = 'none';
    $e[$key]['replace_keywords']['#title_display'] = 'none';

    $title = '';
    if (!empty($e[$key]['value']['#title'])) {
      $title = $e[$key]['value']['#title'];
    }
    elseif (!empty($e[$key]['#title'])) {
      $title = $e[$key]['#title'];
    }

    $table['rows'][$key] = array(
      'data' => array(
        'name' => array(
          'data' => $title,
        ),
        'value' => array(
          'data' => render($e[$key]['value']),
        ),
        'replace_keywords' => array(
          'data' => render($e[$key]['replace_keywords']),
        ),
      ),
    );
  }

  return theme('table', $table);
}

/**
 * Implements theme_layouts_amoeba_layout_settings().
 */
function theme_layouts_amoeba_layout_settings($vars) {
  return theme(
    'layouts_amoeba_layout_settings_main',
    array(
      'layout' => $vars['layout'],
      'settings' => $vars['settings'],
    )
  );
}

/**
 * Implements theme_layouts_amoeba_layout_settings_main().
 */
function theme_layouts_amoeba_layout_settings_main($vars) {
  $markup = drupal_array_get_nested_value($vars['settings'], $vars['parents']);
  $indent_level = $vars['indent_level'];

  $output = '';
  if ($indent_level === 0) {
    $output .= sprintf(
      '<div class="%s %s"><pre>',
      'layouts-amoeba-layout-settings-main',
      drupal_html_class($vars['layout']['child_name'])
    );
  }

  $o_tag = $markup['options']['outer']['tag']['name'] ? $markup['options']['outer']['tag'] : NULL;
  $i_tag = $markup['options']['inner']['tag']['name'] && $o_tag ? $markup['options']['inner']['tag'] : NULL;
  $has_children = isset($markup['children']);

  if ($o_tag) {
    if ($indent_level) {
      $output .= '<br />';
    }

    $attributes = array();
    foreach ($o_tag['attributes'] as $attr_name => $attr) {
      if (!empty($attr['value'])) {
        $attributes[$attr_name] = $attr['value'];
      }
    }
    $attributes = $attributes ? drupal_attributes($attributes) : '';

    $output .= str_repeat('  ', $indent_level) . "&lt;{$o_tag['name']}{$attributes}&gt;";

    if ($has_children) {
      $indent_level++;
    }
  }

  if ($i_tag) {
    $attributes = array();
    foreach ($i_tag['attributes'] as $attr_name => $attr) {
      if (!empty($attr['value'])) {
        $attributes[$attr_name] = $attr['value'];
      }
    }
    $attributes = $attributes ? drupal_attributes($attributes) : '';

    $output .= '<br />' . str_repeat('  ', $indent_level) . "&lt;{$i_tag['name']}{$attributes}&gt;";

    if ($has_children) {
      $indent_level++;
    }
  }

  if ($has_children) {
    foreach (array_keys($markup['children']) as $child_name) {
      $child_vars = $vars;
      $child_vars['indent_level'] = $indent_level;
      array_push($child_vars['parents'], 'children', $child_name);
      $output .= theme_layouts_amoeba_layout_settings_main($child_vars);
    }
  }
  else {
    $region_name = end($vars['parents']);
    $output .= sprintf(
      '<span class="%s">&nbsp;&nbsp;%s&nbsp;&nbsp;</span>',
      'layouts-amoeba-layout-admin-content ' . str_replace('_', '-', $region_name),
      $vars['layout']['regions'][$region_name]
    );
  }

  if ($i_tag) {
    if ($has_children) {
      $indent_level--;
      $output .= '<br />' . str_repeat('  ', $indent_level);
    }

    $output .= "&lt;/{$i_tag['name']}&gt;";
  }

  if ($o_tag) {
    if ($has_children) {
      $indent_level--;
      $output .= '<br />' . str_repeat('  ', $indent_level);
    }

    $output .= "&lt;/{$o_tag['name']}&gt;";
  }

  if ($indent_level === 0) {
    $output .= '</pre></div>';
  }

  return $output;
}
