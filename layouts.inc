<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Initialize the HTML markup settings.
 *
 * @param string $preset_name
 *   Machine name of the layout.
 * @param array $parents
 *   Array of parent identifiers in the original $settings.
 * @param array $settings
 *   HTML markup settings.
 */
function layouts_amoeba_layout_child_settings_main_defaults($preset_name, array $parents, array &$settings) {
  $type = empty($settings['children']) ? 'region' : 'wrapper';

  $base = array(
    'options' => array(
      'variation' => '',
      'outer' => array(
        'tag' => array(
          'name' => 'div',
          'attributes' => array(
            'class' => array(
              'value' => array(),
              'replace_keywords' => FALSE,
            ),
          ),
        ),
        'render_empty' => TRUE,
      ),
      'inner' => array(
        'tag' => array(
          'name' => '',
          'attributes' => array(
            'class' => array(
              'value' => array(),
              'replace_keywords' => FALSE,
            ),
          ),
        ),
        'render_empty' => TRUE,
      ),
    ),
  );

  foreach (array('outer', 'inner') as $position) {
    $base['options'][$position]['tag']['attributes']['class']['value'] = layouts_amoeba_layout_settings_main_defaults_class(
      $preset_name,
      $position,
      $type,
      $parents
    );
  }

  if (count($parents) > 1) {
    // The Id attribute of the most outer wrapper can be configured on the
    // "General" tab in case of Panels or on the {view_mode}/settings page in
    // case of Panelizer.
    // @todo I have not found corresponding settings for MiniPanels.
    $base['options']['outer']['tag']['attributes']['id'] = array(
      'value' => '',
      'replace_keywords' => FALSE,
    );
  }
  else {
    // The "main" is singleton.
    $settings['weight'] = 0;
  }

  $settings += $base;
  $settings['options'] += $base['options'];
  foreach (array('outer', 'inner') as $position) {
    $settings['options'][$position] += $base['options'][$position];
    $settings['options'][$position]['tag'] += $base['options'][$position]['tag'];
    $settings['options'][$position]['tag']['attributes'] += $base['options'][$position]['tag']['attributes'];

    // Add default attributes.
    foreach ($base['options'][$position]['tag']['attributes'] as $attr_name => $attr) {
      $settings['options'][$position]['tag']['attributes'][$attr_name] += $attr;
    }

    // Set the default properties of unknown attributes.
    foreach (array_keys($settings['options'][$position]['tag']['attributes']) as $attr_name) {
      $settings['options'][$position]['tag']['attributes'][$attr_name] += array(
        'replace_keywords' => FALSE,
      );
    }
  }

  if (isset($settings['children'])) {
    $weight = 0;
    foreach ($settings['children'] as $child_name => &$child) {
      $child += array('weight' => $weight++);
      $child_parents = $parents;
      $child_parents[] = $child_name;
      layouts_amoeba_layout_child_settings_main_defaults(
        $preset_name, $child_parents, $child
      );
    }
  }
}

/**
 * Default classes.
 *
 * @param string $preset_name
 *   Example: 'layouts_amoeba:foo'.
 * @param string $position
 *   Expected values are 'outer' or 'inner'.
 * @param string $type
 *   Expected values are 'wrapper' or 'region'.
 * @param array $parents
 *   Machine name of the wrapper or region.
 *
 * @return string[]
 *   Classes.
 */
function layouts_amoeba_layout_settings_main_defaults_class($preset_name, $position, $type, array $parents) {
  $name = end($parents);
  $classes = array();

  if ($position === 'outer') {
    if (count($parents) === 1) {
      $preset_parent = 'layouts-amoeba';
      $preset_child = strtr($preset_name, array('_' => '-'));
      // @todo Don't add the "clearfix" class to outer when the inner is in use.
      $classes[] = 'clearfix';
      $classes[] = $preset_parent;
      $classes[] = $preset_parent . '-' . $preset_child;
    }
    else {
      $classes[] = $type;
      $classes[] = "$type-" . drupal_html_class($name);
    }
  }
  else {
    $classes[] = 'clearfix';
    $classes[] = 'inner';
  }

  return $classes;
}

/**
 * Default values for SVG icon generation.
 *
 * @return array
 *   Keys:
 *   - width:  integer. Number of pixels.
 *   - height: integer. Number of pixels
 *   - gap:    integer. Number of pixels.
 *   - ohgap:  integer. Outer horizontal gap.
 *   - ihgap:  integer. Inner horizontal gap.
 *   - ovgap:  integer. Outer vertical gap.
 *   - ivgap:  integer. Inner vertical gap.
 */
function layouts_amoeba_layout_generate_icon_default_settings() {
  return array(
    'width' => 54,
    'height' => 72,
    'gap' => 2,
    'ohgap' => array(
      1 => 2,
      2 => 2,
      3 => 2,
      4 => 2,
      5 => 2,
    ),
    'ihgap' => array(
      1 => 1,
      2 => 1,
      3 => 1,
      4 => 1,
      5 => 1,
    ),
    'ovgap' => 2,
    'ivgap' => array(
      1 => 1,
      2 => 1,
      3 => 1,
      4 => 1,
    ),
  );
}

/**
 * Generate layout icons for the given modules.
 *
 * @param string[] $modules
 *   Module names.
 * @param string|null $css
 *   CSS to styling SVG objects.
 *
 * @return array
 *   Keys are file system paths, values are SVG content.
 */
function layouts_amoeba_layout_generate_icons(array $modules = array(), $css = NULL) {
  $icons = array();

  ctools_include('plugins', 'panels');
  $layouts = panels_get_layouts();

  foreach ($layouts as $layout) {
    if (!isset($layout['icon'])
      || !isset($layout['layouts_icons'])
      || ($modules && !in_array($layout['module'], $modules))
    ) {
      continue;
    }

    $file_path_default = $layout['path'] . '/' . $layout['icon'];
    $file_path_base = preg_replace('/\.svg$/', '', $file_path_default);
    foreach ($layout['layouts_icons'] as $icon_name => $icon) {
      if ($icon_name === 'default') {
        $file_path = $file_path_default;
      }
      else {
        $file_path = $file_path_base . "-{$icon_name}.svg";
      }

      $icons[$file_path] = layouts_amoeba_layout_generate_icon(
        $icon['boxes'],
        $icon['settings'],
        $css
      );
    }
  }

  return $icons;
}

/**
 * Generate layout icon.
 *
 * @param array $boxes
 *   Box definitions.
 * @param array $settings
 *   Icon settings.
 * @param string $css
 *   CSS to styling SVG objects.
 *
 * @return string
 *   SVG.
 */
function layouts_amoeba_layout_generate_icon(array $boxes, array $settings = array(), $css = NULL) {
  $canvas = layouts_amoeba_layout_generate_icon_canvas($boxes);
  $settings += layouts_amoeba_layout_generate_icon_default_settings();

  if (!$css) {
    $path = drupal_get_path('module', 'layouts');
    $num_of_regions = count($boxes);
    $css_file = "$path/css/layouts.layout-icon-r{$num_of_regions}.css";
    if (is_file($css_file)) {
      $css = file_get_contents($css_file);
    }
  }

  $gap = $settings['gap'];

  $horizontal_pixels = layouts_amoeba_layout_icon_grid_to_pixel(
    $canvas['width'],
    $settings['width'] - ($gap * 2),
    $gap
  );

  $vertical_pixels = layouts_amoeba_layout_icon_grid_to_pixel(
    $canvas['height'],
    $settings['height'] - ($gap * 2),
    $gap
  );

  $doc = new DOMDocument('1.0', 'utf-8');
  $doc->preserveWhiteSpace = TRUE;
  $doc->formatOutput = TRUE;

  $e_svg = $doc->createElement('svg');
  $doc->appendChild($e_svg);

  $e_svg->setAttribute('xmlns', 'http://www.w3.org/2000/svg');

  $e_svg->setAttribute('id', "layouts-amoeba");
  $e_svg->setAttribute('version', '1.1');
  $e_svg->setAttribute('width', $settings['width']);
  $e_svg->setAttribute('height', $settings['height']);

  $e_defs = $doc->createElement('defs');
  $e_svg->appendChild($e_defs);

  $e_style = $doc->createElement('style');
  $e_defs->appendChild($e_style);
  $e_style->setAttribute('type', 'text/css');

  $e_style->appendChild($doc->createCDATASection($css));

  $e_g = $doc->createElement('g');
  $e_svg->appendChild($e_g);

  $stroke_width = 1;
  $half_stroke_width = $stroke_width / 2;
  foreach ($boxes as $region => $box) {
    $e_rect = $doc->createElement('rect');
    $e_g->appendChild($e_rect);

    $x = round($gap + $horizontal_pixels[$box['x']]) + $half_stroke_width;
    $y = round($gap + $vertical_pixels[$box['y']]) + $half_stroke_width;

    if ($box['x']) {
      $x += $gap;
    }

    if ($box['y']) {
      $y += $gap;
    }

    $e_rect->setAttribute('id', "region-$region");
    $e_rect->setAttribute('width', round($horizontal_pixels[$box['width']]) - $stroke_width);
    $e_rect->setAttribute('height', round($vertical_pixels[$box['height']]) - $stroke_width);
    $e_rect->setAttribute('x', $x);
    $e_rect->setAttribute('y', $y);
  }

  return $doc->saveXML();
}

/**
 * Calculate the size of the canvas from the boxes.
 *
 * @param array $boxes
 *   Array of box definitions which are also arrays with the following keys:
 *   - x:      integer.
 *   - y:      integer.
 *   - width:  integer.
 *   - height: integer.
 *
 * @return array
 *   Keys:
 *   - width:  integer.
 *   - height: integer.
 */
function layouts_amoeba_layout_generate_icon_canvas(array $boxes) {
  $canvas = array(
    'width' => 0,
    'height' => 0,
  );

  foreach ($boxes as $box) {
    $canvas['width'] = max($canvas['width'], $box['x'] + $box['width']);
    $canvas['height'] = max($canvas['height'], $box['y'] + $box['height']);
  }

  return $canvas;
}

/**
 * Convert amount of grids to pixels.
 *
 * @param int $grid
 *   Number of grids.
 * @param int $pixel
 *   Total width or height in pixels.
 * @param int $gap
 *   Gap in pixels between the columns or rows.
 *
 * @return array
 *   Key is the number of merged columns, the value is the how wide the merged
 *   columns in pixels.
 */
function layouts_amoeba_layout_icon_grid_to_pixel($grid, $pixel, $gap) {
  $pixels = array(0 => 0);

  $one = ($pixel - ($gap * ($grid - 1))) / $grid;
  for ($i = 1; $i <= $grid; $i++) {
    $pixels[$i] = ($one * $i) + ($gap * ($i - 1));
  }

  return $pixels;
}
