<?php
/**
 * @file
 * Drush integration.
 */

/**
 * Implements hook_drush_help().
 */
function layouts_drush_help($subject) {
  $help = array();

  $args = explode(':', $subject);
  $subject = array_shift($args);
  if ($args) {
    $subject .= ':' . array_shift($args);
  }

  switch ($subject) {
    case 'meta:layouts':
      if ($args[0] == 'title') {
        $help[] = dt('All commands in !category', array('!category' => 'Layouts'));
      }
      elseif ($args[0] == 'summary') {
        $help[] = 'Layouts related drush commands.';
      }
      break;

  }

  return implode("\n", $help);
}

/**
 * Implements hook_drush_command().
 */
function layouts_drush_command() {
  return array(
    'layouts-amoeba-layout-generate-icons' => array(
      'aliases' => array('lalgi'),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'description' => dt('Some helpful description.'),
      'required-arguments' => FALSE,
      'arguments' => array(
        'module' => t('Module'),
      ),
      'allow-additional-options' => FALSE,
      'options' => array(
        'css-file' => array(
          'description' => dt('File system path to a CSS file.'),
          'example-value' => '/path/to/my.css',
          'required' => FALSE,
          'hidden' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Implements drush_COMMAND_validate().
 */
function drush_layouts_amoeba_layout_generate_icons_validate($module = NULL) {
  if ($module && !module_exists($module)) {
    drush_set_error(
      'LAYOUTS_INVALID_MODULE',
      dt('Module "!name" is not exists.', array('!name' => $module))
    );
  }

  $css_file = drush_get_option('css-file');
  if ($css_file && !is_file($css_file)) {
    drush_set_error(
      'LAYOUTS_FILE_NOT_FOUND',
      dt('CSS file "!name" is not exists.', array('!name' => $css_file))
    );
  }

  foreach (array('width', 'height') as $name) {
    $value = drush_get_option($name, NULL);
    if ($value !== NULL && $value != (string) intval($value)) {
      drush_set_error(
        'LAYOUTS_NAN',
        dt('Option "!name" is not numeric. Value = "!value"', array(
          '!name' => $name,
          '!value' => $value,
        ))
      );
    }
  }
}

/**
 * Implements drush_COMMAND().
 */
function drush_layouts_amoeba_layout_generate_icons($module = NULL) {
  module_load_include('inc', 'layouts');

  $modules = $module ? array($module) : array();
  $css_file = drush_get_option('css-file');
  $css = ($css_file ? file_get_contents($css_file) : NULL);

  $svgo = NULL;
  if (drush_shell_exec('which svgo')) {
    $output = drush_shell_exec_output();
    $svgo = trim(reset($output));
  }

  if (!$svgo) {
    drush_log(dt('The `svgo` SVG optimizer is not available'), 'warning');
  }

  $icons = layouts_amoeba_layout_generate_icons($modules, $css);
  foreach ($icons as $file_name => $svg) {
    $directory = pathinfo($file_name, PATHINFO_DIRNAME);
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);

    if (file_put_contents($file_name, $svg)) {
      drush_log($file_name, 'ok');
      if ($svgo) {
        drush_shell_exec_interactive(sprintf('%s %%s', escapeshellcmd($svgo)), $file_name);
      }
    }
    else {
      drush_set_error(
        'LAYOUTS_FILE_SAVE',
        dt('Failed to save file "@file"', array('@file' => $file_name))
      );
    }
  }

}
